import '../App.css';
import {Navbar, Container, Nav,} from 'react-bootstrap';
import {Link, NavLink} from 'react-router-dom';
import {useContext} from 'react';
import UserContext from '../UserContext.js';

export default function AppNavbar(){
	const {user} = useContext(UserContext);
	return(
		<Navbar className="grad shadow " expand="lg">
			<Container fluid>
				<Navbar.Brand className="cu-fo" as={Link} to='/'>Fishda</Navbar.Brand>

				<Navbar.Toggle aria-controls="basic-navbar-nav"/>
				<Navbar.Collapse id="basic-navbar-nav">
					<Nav className="ms-auto mont-fo ">
						<Nav.Link className='mont-foc' as={NavLink} to='/'>Home</Nav.Link>
						<Nav.Link className='mont-foc'as={NavLink} to='/courses'>Products</Nav.Link>
						{ (user.id !== null) ?
				    			user.isAdmin ?
				    				<>
					    				<Nav.Link className='mont-foc' as={NavLink} to='/courses/add'>Add Fish</Nav.Link>
					    				<Nav.Link className='mont-foc' as={NavLink} to='/logout'>Logout</Nav.Link>
					    			</>
					    		:
					    			<>
					    				<Nav.Link className='mont-foc' as={NavLink} to='/profile'>Profile</Nav.Link>
					    				<Nav.Link className='mont-foc' as={NavLink} to='/logout'>Logout</Nav.Link>
					    			</>
			    			:	
			    				<>
						    		<Nav.Link className='mont-foc' as={NavLink} to='/register'>Register</Nav.Link>
						    		<Nav.Link className='mont-foc' as={NavLink} to='/login'>Login</Nav.Link>
			    				</>		
			    		}
					</Nav>
				</Navbar.Collapse>
			</Container>    
		</Navbar>
	)
}