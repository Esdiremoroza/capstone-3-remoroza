import {Row, Col, Card, Button} from 'react-bootstrap';
import PropTypes from 'prop-types';
import { useState } from 'react';
import { Link } from 'react-router-dom';

export default function CourseCard({course}){
	
	const {_id, name, description, price}=course;

	return(
			<Row className='my-3'>
				<Col xs={12}>
					<Card className="cardHighlight p-3" id="courseComponent1">
						<Card.Body>
							<Card.Title>{name}</Card.Title>

							<Card.Subtitle>Description:</Card.Subtitle>
							<Card.Text>{description}</Card.Text>

							<Card.Subtitle>Price:</Card.Subtitle>
							<Card.Text>{price}</Card.Text>

							<Link className="btn btn-primary" to={`/courses/${_id}`}> View details </Link>
						</Card.Body>
					</Card>
				</Col>
			</Row>
		)
}

CourseCard.propTypes={
	course:PropTypes.shape({
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired
	})
}