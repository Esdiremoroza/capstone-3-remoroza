import {Table, Button} from 'react-bootstrap';
import {useState, useEffect} from 'react';
import EditCourse from './EditCourse.js';
import ArchiveCourse from './ArchiveCourse.js';

export default function AdminView({coursesData, fetchCourses}){
	
	const [courses, setCourses]=useState([])

	useEffect(()=>{
		const courses_array = coursesData.map(course=> {
			return(
					<tr key={course._id}>
						<td>{course._id}</td>
						<td>{course.name}</td>
						<td>{course.description}</td>
						<td>{course.price}</td>
						<td>{course.isActive?'Available':'Unavailable'}</td>
						<td>
							<EditCourse course_id={course._id} fetchCourses={fetchCourses}/>
						</td>
						<td>
							<ArchiveCourse course_id={course._id} fetchCourses={fetchCourses} isActive={course.isActive}/>
						</td>
					</tr>
				)
		})
		setCourses(courses_array)
	},[coursesData])

	return(

			<>
				<h1 className="cu-fo00 text-center py-3">
					Admin Dashboard
				</h1>
				<Table striped bordered hover responsive>
					<thead>
						<tr className="cu-fo200">
							<th>ID</th>
							<th>Name</th>
							<th>Description</th>
							<th>Price</th>
							<th>Availability</th>
							<th>Actions</th>
						</tr>
					</thead>
					<tbody className="cu-fo200">
						{courses}
					</tbody>
				</Table>
			</>

		)
}