export default function NotFound(){
	return(
			<>
				<h1>
				Not Found
				</h1>
				<p>The page you visited could not be found</p>
			</>
		)
}