import {useState, useEffect , useContext} from 'react';
import {Form, Button} from 'react-bootstrap';
import {Navigate, useNavigate} from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext.js'

export default function AddCourse() {
	const navigate = useNavigate();
	const {user} = useContext(UserContext);

	const [name, setName] = useState ("")
	const [description, setDescription] = useState ("")
	const [price, setPrice] = useState (0)

	function createCourse(event){
		event.preventDefault();

		let token= localStorage.getItem('token');

		fetch (`${process.env.REACT_APP_API_URL}/api/courses/`,{
			method:'POST',
			headers:{
				'Content-Type':'application/json',
				Authorization:`Bearer ${token}`
			},
			body:JSON.stringify({
				name: name,
				description: description,
				price: price
			})
		})
		.then(response=> response.json())
		.then(result=>{
			if (result){
				Swal.fire({
					title:'New Course Added',
					icon: 'success'
				})
				setName('')
				setDescription('')
				setPrice(0)

				navigate('/courses')
			}else{
				Swal.fire({
					title:'New Course Added',
					text: 'Course creation unsuccessful',
					icon: 'error'
				})
			}
		})
	}

	return(
			(user.isAdmin==true)?
				<>
				 	<h1 className="my-4 text-center cu-fo2010">Add Fish</h1>
	                <Form onSubmit={event => createCourse(event)}>
	                    <Form.Group>
	                        <Form.Label>Name:</Form.Label>
	                        <Form.Control type="text" placeholder="Enter Name" required value={name} onChange={event => {setName(event.target.value)}}/>
	                    </Form.Group>
	                    <Form.Group>
	                        <Form.Label>Description:</Form.Label>
	                        <Form.Control type="text" placeholder="Enter Description" required value={description} onChange={event => {setDescription(event.target.value)}}/>
	                    </Form.Group>
	                    <Form.Group>
	                        <Form.Label>Price:</Form.Label>
	                        <Form.Control type="number" placeholder="Enter Price" required value={price} onChange={event => {setPrice(event.target.value)}}/>
	                    </Form.Group>
	                    <Button variant="primary" type="submit" className="my-5">Submit</Button>
	                </Form>
				</>
			:
				<Navigate to='/courses'/>

		)
}